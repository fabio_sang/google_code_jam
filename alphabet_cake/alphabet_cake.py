def get_letter(row, start=0):
    for index, cell in enumerate(row):
        if cell != '?' and index >= start:
            return cell, index
    return None, len(matrix[0])


def get_row_with_letters(start=0):
    if start >= len(matrix):
        return len(matrix)
    curr_row_index = start + 1
    while curr_row_index < len(matrix) and all(cell == '?' for cell in matrix[curr_row_index]):
        curr_row_index += 1
    return curr_row_index


def fill_rectangle(letter, start_row_index, end_row_index, start_col_index, end_col_index):
    curr_row_index = start_row_index
    while curr_row_index <= end_row_index:
        matrix[curr_row_index][start_col_index:end_col_index] = letter*(end_col_index - start_col_index)
        curr_row_index += 1


def compute_alphabet_cake(matrix):
    curr_row_index = 0
    first_letter_of_matrix = True  # Se non ci sono lettere nelle prime righe, parto dalla prima riga a riempire di lettere
    while curr_row_index < len(matrix):
        curr_row = matrix[curr_row_index]
        next_row_index = get_row_with_letters(curr_row_index)
        curr_letter, curr_letter_index = get_letter(curr_row)
        first_letter_of_row = True  # Se non ci sono lettere nelle prime colonne, parto dalla prima colonna a riempire di lettere
        while curr_letter_index < len(matrix[0]):
            next_letter, next_letter_index = get_letter(curr_row, curr_letter_index + 1)
            if first_letter_of_row:
                curr_letter_index = 0
            if first_letter_of_matrix:
                curr_row_index = 0
            fill_rectangle(curr_letter, curr_row_index, next_row_index - 1, curr_letter_index, next_letter_index)
            curr_letter, curr_letter_index = next_letter, next_letter_index
            first_letter_of_matrix = False
            first_letter_of_row = False
        curr_row_index = next_row_index
    return '\n'.join([''.join([cell for cell in row]) for row in matrix])


matrices = list()
with open('A-large-practice.in', 'r') as f:
    next(f)
    while True:
        matrix = list()
        dimensions = f.readline()
        if not dimensions:
            break
        rows = int(dimensions.split(' ')[0])
        for i in range(rows):
            matrix.insert(i, list(f.readline().replace('\n', '')))
        matrices.append(compute_alphabet_cake(matrix))

with open ('output-large.txt', 'w') as f:
    for i, m in enumerate(matrices):
        f.writelines(f'Case #{i+1}:\n')
        f.writelines(m)
        f.writelines('\n')
